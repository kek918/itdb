---
Title: Still delivering Out Of Office warning even though it's turned off
Description: Still delivering Out Of Office warning even though it's turned off
Author: Kim Eirik Kvassheim
Date: 2014-08-13
Template: post
Tags: outlook, office, exchange
---

## Still delivering Out Of Office warning even though it's turned off

This seems to be a recurring problem for some users every year.

We've tried:

- Start Outlook.exe by using outlook.exe /safe and/or outlook.exe /resetnavpane
- Check that OOF is indeed turned off both in the Outlook client but also in Exchange Web Admin tools (ECP)

There seems to be some issue when turning off OOF client-side in the Windows registry.
Luckily there's a tool called MFCMAPI to resolve this.

Step 1:
Download MFCMAPI at their website, http://mfcmapi.codeplex.com
Make sure to download the same architecture as your Outlook is running. 64 bit MFCMAPI won't work with 32 bit Outlook.

Step 2:
Close Outlook and open mfcmapi.exe.
Click on Session -> Logon in the top menu.
Chose the correct Outlook profile.

Step 3:
Chose your account in the list on the top. See example below: My chosen account has a blue background color:
![MFCMAPI 1](/itdb/assets/out-of-office.png)

Step 4:
Scroll down in the property-list until you see the property called PR_OOF_STATE, PidTagOutOfOffice...... this will be set to True if Out Of Office is activated.
![MFCMAPI 2](/itdb/assets/out-of-office2.png)

Step 5:
Double click on this property, then remove the checkbox from the Boolean field and click OK.
![MFCMAPI 3](/itdb/assets/out-of-office3.png)

Click on Session -> Log Off in the top menu.

Now Out of Office is finally deactivated.

<!--
http://it-db.com/article/still-delivering-outofoffice-warning-even-though-its-turned-off
-->

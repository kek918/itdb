---
Title: Restore Word 2013 templates
Description: Restore Word 2013 templates
Author: Kim Eirik Kvassheim
Date: 10.03.2016
Template: post
Tags: word 2013,office
---

## Restore Word 2013 templates

Solution: reset the save path for custom Templates in Office

Open Word 2013 and open File -> Options

From here on you have two choices

1: Open the *Save* tab and change the `Default personal templates location:` field to match the folder where your templates are saved.

2: Open the *Advanced* tab and scroll down to *General*. From here click on the *File Locations...* button and you can modify both `User templates` and `Workgroup templates`

Normally your templates can be found in `%appdata%\Microsoft\Templates`

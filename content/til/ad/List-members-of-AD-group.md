---
Title: List members of AD group
Description: List members of AD group
Author: Kim Eirik Kvassheim
Date: 04.12.2013
Template: post
Tags: active directory, ad
---

## List members of AD group

You can use this cmdlet to list all objects (users, groups and computers) in the specified group.
Open Powershell shell (pun intended) on your domain controller or Active Directory server and run the following command:

```
Get-ADGroupMember 'groupname'
```

You can also export the output to a text file:

```
Get-ADGroupMember 'groupname' > list.txt
```

If needed, you may pipe the output to contain only "name" from the AD group:

```
Get-ADGroupMember 'groupname' | select name > list.txt
```

Other possible columns are `distinguishedName`, `objectClass`, `objectGUID`, `SamAccountName` and `SID`.
You can comma seperate several columns to output what you need.

<!--
http://it-db.com/article/list-members-of-active-directory-group-to-text-file
-->

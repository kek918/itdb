---
Title: Find MAC address with arp command
Description: Find MAC address with arp command
Author: Kim Eirik Kvassheim
Date: 22.03.2016
Template: post
Tags: network, cmd
---

## Find MAC address with arp command

Show all cached machines on your network with IP address and MAC address:

`arp -a`

Search for a specific machine with IP address:

```
arp -a | find "192.168.20.149"    # windows
arp -a | grep "192.168.20.149"    # linux
```

The output will tell you the MAC address belonging to the IP address if it's cached in your computer, or else it'll return empty.

Very useful if you need to send a remote WakeOnLAN packet but don't know the MAC address of the remote computer.

<!--
http://it-db.com/article/find-ip-address-by-mac-and-vice-versa#.VvEDVyYo9X8
-->

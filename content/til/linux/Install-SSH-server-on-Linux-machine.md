---
Title: Install SSH server on Linux machine
Description: Install SSH server on Linux machine
Author: Kim Eirik Kvassheim
Date: 18.03.2016
Template: post
Tags: ssh,openssh,linux
---

## Install SSH server on Linux machine

The following example are based on Debian.

Install SSH Server:
```
$ sudo apt-get install openssh-server
```

Control OpenSSH server:
```
$ sudo /etc/init.d/ssh start
$ sudo service ssh start

$ sudo /etc/init.d/ssh stop
$ sudo service ssh stop

$ sudo /etc/init.d/ssh restart
$ sudo service ssh restart

$ sudo /etc/init.d/ssh status
$ sudo service ssh status
```

Connect to a SSH server:
```
ssh user@host
```

For more information about OpenSSH you can read the FAQ here: http://www.openssh.com/faq.html

<!--
http://it-db.com/article/how-to-remote-control-your-linux-machine-with-ssh
-->

---
Title: How to password protect your page with .htaccess
Description: How to password protect your page with .htaccess
Author: Kim Eirik Kvassheim
Date: 2012-04-11
Template: post
Tags: htpasswd, apache
---

## How to password protect your page with .htaccess


### Step 1 - create a htpasswd file

[http://www.htaccesstools.com/htpasswd-generator/](http://www.htaccesstools.com/htpasswd-generator/)

Copy the output to your text editor and save it as .htpasswd.
Upload the .htpasswd to your webbrowser. I recommend to upload it to a "hidden" or secret folder instead of the root folder where people can find it.

Example .htpasswd file:
```
kek:nv1irO7LNuRoY
someone:66KKbCZZmwzUU
guy:36hmT4aexfjTY
```


### Step 2 - activate password protection in htaccess

Create a new file in your text editor and paste the code below:
```
AuthUserFile /path/to/.htpasswd
AuthType Basic
AuthName "Protected folder"
Require valid-user
```

Change the `/path/to/.htpasswd` to the path where your file is.
The `Protected folder` string is the message that will popup in the password box. Change this to whatever you like.

This code will protect the folder where you put the .htaccess and all subdirectories.



<!--
http://it-db.com/article/how-to-password-protect-your-page-with-htaccess#.VvED2CYo9X8
-->

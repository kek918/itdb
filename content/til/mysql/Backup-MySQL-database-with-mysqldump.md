---
Title: Backup MySQL database with mysqldump
Description: Backup MySQL database with mysqldump
Author: Kim Eirik Kvassheim
Date: 28.09.2012
Robots: noindex,nofollow
Template: post
Tags: mysql, database
---

## Backup MySQL database with mysqldump

The syntax:
```
mysqldump -u <username> -p<password> <dbname> > <outputfile>
```

Note: There cant be any spaces between -p and the password. Alternatively you can omit the password and keep the `-p` parameter to make mysqldump prompt you for a password.

To backup specific table(s):
```
mysqldump -u root -p database table1 table2 table99 > tables.sql
```

Additionally, you can restore a .sql file like this:
```
mysql -u <username> -p<password> <dbname> < file.sql
```

<!--
http://it-db.com/article/how-to-backup-your-database-using-mysqldump-or-phpmyadmin
-->
